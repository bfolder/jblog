package com.boxedfolder.jblog.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    @RequestMapping("/")
    public String showIndex() {
        return "index";
    }

    @RequestMapping("/swagger")
    public String showSwagger() {
        return "swagger/index";
    }
}
