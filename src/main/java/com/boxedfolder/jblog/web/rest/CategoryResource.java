package com.boxedfolder.jblog.web.rest;

import com.boxedfolder.jblog.domain.Category;
import com.boxedfolder.jblog.service.CategoryService;
import com.wordnik.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "categories", description = "Endpoint for categories")
@RestController
@RequestMapping("/rest/categories")
public class CategoryResource extends CrudResource<CategoryService, Category> {
}