package com.boxedfolder.jblog.web.rest;

import com.boxedfolder.jblog.domain.Comment;
import com.boxedfolder.jblog.service.CommentService;
import com.wordnik.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "comments", description = "Endpoint for comments")
@RestController
@RequestMapping("/rest/comments")
public class CommentResource extends CrudResource<CommentService, Comment> {
}