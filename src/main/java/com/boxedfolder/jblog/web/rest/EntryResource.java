package com.boxedfolder.jblog.web.rest;

import com.boxedfolder.jblog.domain.Entry;
import com.boxedfolder.jblog.service.EntryService;
import com.wordnik.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "entries", description = "Endpoint for entries")
@RestController
@RequestMapping("/rest/entries")
public class EntryResource extends CrudResource<EntryService, Entry> {
}