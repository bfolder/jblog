package com.boxedfolder.jblog.config.security;

import com.boxedfolder.jblog.config.security.service.UserDetailService;
import com.boxedfolder.jblog.config.security.xauth.XAuthTokenConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.SecurityConfigurer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.DefaultSecurityFilterChain;

import javax.inject.Inject;

@EnableWebMvcSecurity
@EnableWebSecurity(debug = true)
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private UserProperties userProperties;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        String[] securedEndpoints = {"entries", "categories"};
        for (String endpoint : securedEndpoints) {
            http.authorizeRequests().antMatchers(HttpMethod.POST, "/rest/" + endpoint + "/**").hasRole(UserDetailService.ROLE_ADMIN);
            http.authorizeRequests().antMatchers(HttpMethod.DELETE, "/rest/" + endpoint + "/**").hasRole(UserDetailService.ROLE_ADMIN);
            http.authorizeRequests().antMatchers(HttpMethod.PUT, "/rest/" + endpoint + "/**").hasRole(UserDetailService.ROLE_ADMIN);
            http.authorizeRequests().antMatchers(HttpMethod.PATCH, "/rest/" + endpoint + "/**").hasRole(UserDetailService.ROLE_ADMIN);
        }
        /*
        http.authorizeRequests().antMatchers(HttpMethod.DELETE, "/rest/categories/**").hasRole(UserDetailService.ROLE_ADMIN);
        http.authorizeRequests().antMatchers(HttpMethod.PATCH, "/rest/categories/**").hasRole(UserDetailService.ROLE_ADMIN);
        http.authorizeRequests().antMatchers(HttpMethod.PUT, "/rest/categories/**").hasRole(UserDetailService.ROLE_ADMIN);
        */

        SecurityConfigurer<DefaultSecurityFilterChain, HttpSecurity> securityConfigurerAdapter = new XAuthTokenConfigurer(userDetailsServiceBean());
        http.apply(securityConfigurerAdapter);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder authManagerBuilder) throws Exception {
        authManagerBuilder.userDetailsService(new UserDetailService(userProperties));
    }


    @Bean
    @Override
    public UserDetailsService userDetailsServiceBean() throws Exception {
        return super.userDetailsServiceBean();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Inject
    public void setUserProperties(UserProperties userProperties) {
        this.userProperties = userProperties;
    }
}
