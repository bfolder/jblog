package com.boxedfolder.jblog.repository;

import com.boxedfolder.jblog.domain.Comment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface CommentRepository extends CrudRepository<Comment, Long> {
}
