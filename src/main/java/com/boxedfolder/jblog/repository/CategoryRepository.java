package com.boxedfolder.jblog.repository;

import com.boxedfolder.jblog.domain.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface CategoryRepository extends CrudRepository<Category, Long> {
}
