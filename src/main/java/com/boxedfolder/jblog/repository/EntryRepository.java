package com.boxedfolder.jblog.repository;

import com.boxedfolder.jblog.domain.Entry;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface EntryRepository extends CrudRepository<Entry, Long> {
    List<Entry> findAllByOrderByDateCreatedDesc();
}
