package com.boxedfolder.jblog.service;

import com.boxedfolder.jblog.exceptions.NotFoundException;

import java.util.List;

public interface CrudService<T> {
    List<T> getAll();
    T save(T object);
    T get(Long objectId) throws NotFoundException;
    void delete(Long objectId) throws NotFoundException;
}
