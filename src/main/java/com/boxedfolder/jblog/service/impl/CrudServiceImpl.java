package com.boxedfolder.jblog.service.impl;

import com.boxedfolder.jblog.exceptions.NotFoundException;
import com.boxedfolder.jblog.service.CrudService;
import org.springframework.data.repository.CrudRepository;

import javax.inject.Inject;
import java.util.List;

public abstract class CrudServiceImpl<T, S extends CrudRepository<T, Long>> implements CrudService<T> {
    protected S repository;

    @Override
    public List<T> getAll() {
        return (List<T>)(repository.findAll());
    }

    @Override
    public T save(T object) {
        return repository.save(object);
    }


    @Override
    public T get(Long id) throws NotFoundException {
        T object = repository.findOne(id);
        if (object == null) {
            throw new NotFoundException();
        }

        return object;
    }

    @Override
    public void delete(Long id) throws NotFoundException {
        T object = repository.findOne(id);
        if (object == null) {
            throw new NotFoundException();
        }

        repository.delete(object);
    }

    @Inject
    public void setRepository(S repository) {
        this.repository = repository;
    }
}
