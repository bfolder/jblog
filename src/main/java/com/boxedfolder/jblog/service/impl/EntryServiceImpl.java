package com.boxedfolder.jblog.service.impl;

import com.boxedfolder.jblog.domain.Entry;
import com.boxedfolder.jblog.repository.EntryRepository;
import com.boxedfolder.jblog.service.EntryService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class EntryServiceImpl extends CrudServiceImpl<Entry, EntryRepository> implements EntryService {
    @Override
    public Entry save(Entry object) {
        if (object.getDateCreated() == null) {
            object.setDateCreated(new Date());
        }
        object.setDateUpdated(new Date());

        return repository.save(object);
    }

    @Override
    public List<Entry> getAll() {
        return (List<Entry>)repository.findAllByOrderByDateCreatedDesc();
    }
}
