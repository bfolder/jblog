package com.boxedfolder.jblog.service.impl;

import com.boxedfolder.jblog.domain.Comment;
import com.boxedfolder.jblog.repository.CommentRepository;
import com.boxedfolder.jblog.service.CommentService;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl extends CrudServiceImpl<Comment, CommentRepository> implements CommentService {
}
