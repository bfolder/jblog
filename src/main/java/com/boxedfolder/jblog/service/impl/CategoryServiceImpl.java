package com.boxedfolder.jblog.service.impl;

import com.boxedfolder.jblog.domain.Category;
import com.boxedfolder.jblog.repository.CategoryRepository;
import com.boxedfolder.jblog.service.CategoryService;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl extends CrudServiceImpl<Category, CategoryRepository> implements CategoryService {
}
