package com.boxedfolder.jblog.service;

import com.boxedfolder.jblog.domain.Comment;

public interface CommentService extends CrudService<Comment> {
}
