package com.boxedfolder.jblog.service;

import com.boxedfolder.jblog.domain.Category;

public interface CategoryService extends CrudService<Category> {
}
