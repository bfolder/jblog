package com.boxedfolder.jblog.service;

import com.boxedfolder.jblog.domain.Entry;

public interface EntryService extends CrudService<Entry> {
}

