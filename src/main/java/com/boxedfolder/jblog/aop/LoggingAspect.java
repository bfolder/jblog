package com.boxedfolder.jblog.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class LoggingAspect {
    @Pointcut("execution(* com.boxedfolder.jblog.web.rest.*.*(..))")
    public void resourceMethod() {
    }

    @Before("resourceMethod()")
    public void logResourceMethod(JoinPoint joinPoint) {
        Logger log = LoggerFactory.getLogger(joinPoint.getClass());
        log.info("\nREST request with signature: \n" + joinPoint.getSignature() + "\nArguments: " + Arrays.toString(joinPoint.getArgs()) + "\n");
    }
}
