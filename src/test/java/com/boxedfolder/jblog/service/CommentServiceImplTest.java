package com.boxedfolder.jblog.service;

import com.boxedfolder.jblog.domain.Comment;
import com.boxedfolder.jblog.repository.CommentRepository;
import com.boxedfolder.jblog.service.impl.CommentServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {
    @Mock
    private CommentRepository repository;
    private CommentServiceImpl service;
    private List<Comment> testData;

    @Before
    public void setUp() {
        service = new CommentServiceImpl();
        service.setRepository(repository);
        testData = new ArrayList<Comment>();
        // Create 3 comments
        Comment comment = new Comment();
        comment.setText("Category1");
        comment.setAuthorName("author");
        testData.add(comment);

        comment = new Comment();
        comment.setText("Category2");
        comment.setAuthorName("author");
        testData.add(comment);

        comment = new Comment();
        comment.setText("Category3");
        comment.setAuthorName("author");
        testData.add(comment);
    }

    @Test
    public void testRetrieveAllComments() {
        when(repository.findAll()).thenReturn(testData);
        List<Comment> comments = service.getAll();
        assertEquals(comments, testData);
    }

    @Test
    public void testRetrieveSingleComment() {
        when(repository.findOne(1L)).thenReturn(testData.get(0));
        Comment comment = service.get(1L);
        assertEquals(comment, testData.get(0));
    }

    @Test
    public void testSaveComment() {
        when(repository.save(testData.get(0))).thenReturn(testData.get(0));
        Comment comment = service.save(testData.get(0));
        assertEquals(comment, testData.get(0));
    }
}
