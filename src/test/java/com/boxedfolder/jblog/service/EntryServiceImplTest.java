package com.boxedfolder.jblog.service;

import com.boxedfolder.jblog.domain.Entry;
import com.boxedfolder.jblog.repository.EntryRepository;
import com.boxedfolder.jblog.service.impl.EntryServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EntryServiceImplTest {
    @Mock
    private EntryRepository repository;
    private EntryServiceImpl service;
    private List<Entry> testData;

    @Before
    public void setUp() {
        service = new EntryServiceImpl();
        service.setRepository(repository);
        testData = new ArrayList<Entry>();
        // Create 3 entries
        Entry entry = new Entry();
        testData.add(entry);

        entry = new Entry();
        testData.add(entry);

        entry = new Entry();
        testData.add(entry);
    }

    @Test
    public void testRetrieveAllEntries() {
        when(repository.findAllByOrderByDateCreatedDesc()).thenReturn(testData);
        List<Entry> entries = service.getAll();
        assertEquals(entries, testData);
    }

    @Test
    public void testRetrieveSingleEntry() {
        when(repository.findOne(1L)).thenReturn(testData.get(0));
        Entry entry = service.get(1L);
        assertEquals(entry, testData.get(0));
    }

    @Test
    public void testSaveEntry() {
        when(repository.save(testData.get(0))).thenReturn(testData.get(0));
        Entry entry = service.save(testData.get(0));
        assertEquals(entry, testData.get(0));
        assertNotNull(entry.getDateCreated());
        assertNotNull(entry.getDateUpdated());
    }
}
