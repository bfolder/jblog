package com.boxedfolder.jblog.service;

import com.boxedfolder.jblog.domain.Category;
import com.boxedfolder.jblog.repository.CategoryRepository;
import com.boxedfolder.jblog.service.impl.CategoryServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceImplTest {
    @Mock
    private CategoryRepository repository;
    private CategoryServiceImpl service;
    private List<Category> testData;

    @Before
    public void setUp() {
        service = new CategoryServiceImpl();
        service.setRepository(repository);
        testData = new ArrayList<Category>();
        // Create 3 categories
        Category category = new Category();
        category.setName("Category1");
        testData.add(category);

        category = new Category();
        category.setName("Category2");
        testData.add(category);

        category = new Category();
        category.setName("Category3");
        testData.add(category);
    }

    @Test
    public void testRetrieveAllCategories() {
        when(repository.findAll()).thenReturn(testData);
        List<Category> categories = service.getAll();
        assertEquals(categories, testData);
    }

    @Test
    public void testRetrieveSingleCategory() {
        when(repository.findOne(1L)).thenReturn(testData.get(0));
        Category category = service.get(1L);
        assertEquals(category, testData.get(0));
    }

    @Test
    public void testSaveCategory() {
        when(repository.save(testData.get(0))).thenReturn(testData.get(0));
        Category category = service.save(testData.get(0));
        assertEquals(category, testData.get(0));
    }
}
