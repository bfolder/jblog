package com.boxedfolder.jblog.web;

import com.boxedfolder.jblog.domain.Entry;
import com.boxedfolder.jblog.service.EntryService;
import com.boxedfolder.jblog.web.rest.EntryResource;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class EntryResourceTest {
    @Mock
    private EntryService service;
    private MockMvc restUserMockMvc;
    private List<Entry> testData;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        EntryResource resource = new EntryResource();
        resource.setService(service);
        restUserMockMvc = MockMvcBuilders.standaloneSetup(resource).build();
        // Create 3 entries
        testData = new ArrayList<Entry>();
        Entry entry = new Entry();
        entry.setText("Testtext");
        entry.setTitle("Testtitle");
        entry.setDateUpdated(new Date());
        entry.setDateUpdated(new Date());
        testData.add(entry);

        entry = new Entry();
        entry.setText("Testtext2");
        entry.setTitle("Testtitle2");
        entry.setDateUpdated(new Date());
        entry.setDateUpdated(new Date());
        testData.add(entry);

        entry = new Entry();
        entry.setText("Testtext3");
        entry.setTitle("Testtitle3");
        entry.setDateUpdated(new Date());
        entry.setDateUpdated(new Date());
        testData.add(entry);
    }

    @Test
    public void testGetAllEntries() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writeValueAsString(testData);

        when(service.getAll()).thenReturn(testData);
        restUserMockMvc.perform(get("/rest/entries")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(value));
    }

    @Test
    public void testAddEntry() throws Exception {
        Entry entry = testData.get(0);
        entry.setDateCreated(new Date());
        entry.setDateUpdated(new Date());
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writeValueAsString(entry);
        given(service.save((Entry)notNull())).willReturn(entry);
        restUserMockMvc.perform(post("/rest/entries")
                .content(value)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().string(value));
    }

    @Test
    public void testDeleteEntry() throws Exception {
        restUserMockMvc.perform(delete("/rest/entries/0"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetSingleEntry() throws Exception {
        Entry entry = testData.get(0);
        given(service.get(1L)).willReturn(entry);
        entry.setDateCreated(new Date());
        entry.setDateUpdated(new Date());
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writeValueAsString(entry);
        restUserMockMvc.perform(get("/rest/entries/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(value));
    }
}
