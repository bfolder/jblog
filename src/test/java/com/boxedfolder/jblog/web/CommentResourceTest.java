package com.boxedfolder.jblog.web;

import com.boxedfolder.jblog.domain.Comment;
import com.boxedfolder.jblog.domain.Entry;
import com.boxedfolder.jblog.service.CommentService;
import com.boxedfolder.jblog.web.rest.CommentResource;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(MockitoJUnitRunner.class)
public class CommentResourceTest {
    @Mock
    private CommentService service;
    private MockMvc restUserMockMvc;
    private List<Comment> testData;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CommentResource resource = new CommentResource();
        resource.setService(service);
        restUserMockMvc = MockMvcBuilders.standaloneSetup(resource).build();
        // Create 3 comments
        testData = new ArrayList<Comment>();
        Comment comment = new Comment();
        comment.setText("Category1");
        comment.setAuthorName("author");
        comment.setEntry(new Entry());
        testData.add(comment);

        comment = new Comment();
        comment.setText("Category2");
        comment.setAuthorName("author");
        comment.setEntry(new Entry());
        testData.add(comment);

        comment = new Comment();
        comment.setText("Category3");
        comment.setAuthorName("author");
        comment.setEntry(new Entry());
        testData.add(comment);
    }

    @Test
    public void testGetAllComments() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writeValueAsString(testData);

        when(service.getAll()).thenReturn(testData);
        restUserMockMvc.perform(get("/rest/comments")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(value));
    }

    @Test
    public void testAddComment() throws Exception {
        Comment comment = testData.get(0);
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writeValueAsString(comment);
        given(service.save((Comment)notNull())).willReturn(comment);
        restUserMockMvc.perform(post("/rest/comments")
                .content(value)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().string(value));
    }

    @Test
    public void testDeleteCategory() throws Exception {
        restUserMockMvc.perform(delete("/rest/comments/0"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetSingleCategory() throws Exception {
        Comment comment = testData.get(0);
        given(service.get(1L)).willReturn(comment);
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writeValueAsString(comment);
        restUserMockMvc.perform(get("/rest/comments/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(value));
    }
}
