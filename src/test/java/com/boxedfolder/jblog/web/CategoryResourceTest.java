package com.boxedfolder.jblog.web;

import com.boxedfolder.jblog.domain.Category;
import com.boxedfolder.jblog.service.CategoryService;
import com.boxedfolder.jblog.web.rest.CategoryResource;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class CategoryResourceTest {
    @Mock
    private CategoryService service;
    private MockMvc restUserMockMvc;
    private List<Category> testData;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CategoryResource resource = new CategoryResource();
        resource.setService(service);
        restUserMockMvc = MockMvcBuilders.standaloneSetup(resource).build();
        // Create 3 categories
        testData = new ArrayList<Category>();
        Category category = new Category();
        category.setName("Category1");
        testData.add(category);

        category = new Category();
        category.setName("Category2");
        testData.add(category);
        testData.add(category);

        category = new Category();
        category.setName("Category3");
        testData.add(category);
        testData.add(category);
    }

    @Test
    public void testGetAllCategories() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writeValueAsString(testData);

        when(service.getAll()).thenReturn(testData);
        restUserMockMvc.perform(get("/rest/categories")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(value));
    }

    @Test
    public void testAddCategory() throws Exception {
        Category category = testData.get(0);
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writeValueAsString(category);
        given(service.save((Category)notNull())).willReturn(category);
        restUserMockMvc.perform(post("/rest/categories")
                .content(value)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().string(value));
    }

    @Test
    public void testDeleteCategory() throws Exception {
        restUserMockMvc.perform(delete("/rest/categories/0"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetSingleCategory() throws Exception {
        Category category = testData.get(0);
        given(service.get(1L)).willReturn(category);
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writeValueAsString(category);
        restUserMockMvc.perform(get("/rest/categories/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(value));
    }
}
